#include <iostream>
#include <cstdint>
#include <cstdlib>
#include <bitset>

struct Test
{
	enum e_stone : int_fast16_t	
	{
		S_EMPTY,
		S_WHITE,
		S_BLACK
	};
/*	int_fast32_t	b : 32;
	e_stone			a : 2;*/
	int_fast8_t		a : 1;
	int_fast8_t		b : 8;
};

int	main()
{
	Test	test;

	test.a = 0;
	test.b = 512;
	std::cout << sizeof(test) << std::endl;
//	std::cout << sizeof(test.a) << std::endl;
//	std::cout << sizeof(test.b) << std::endl;

/*	for (int i = 0; i < sizeof(test) * 8; ++i)
	{
		std::cout <<  ((1 << i) & test);
	}*/
//	std::bitset<16> a{ test };

//	std::cout <<  << std::endl;
	getchar();
	return 0;
}
