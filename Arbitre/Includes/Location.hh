#pragma	once

#include <cstdint>

struct Location
{
public:
	enum e_stone : int_fast8_t
	{
		S_EMPTY,
		S_WHITE,
		S_BLACK
	};
public:
	Location();
	~Location();

public:
	e_stone	stone;
};

Location::Location() :
stone(S_EMPTY)
{
}

Location::~Location()
{
}