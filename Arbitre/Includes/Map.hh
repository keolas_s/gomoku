#pragma	once

#include "Location.hh"

class Map
{
	static const	uint_fast8_t HEIGHT = 19;
	static const	uint_fast8_t WIDTH = 19;

public:
	Map();
	~Map();

	Location	*operator[](const uint_fast8_t & height);
private:
	Location	*_map;
};

Map::Map():
_map(new Location[HEIGHT * WIDTH])
{

}

Map::~Map()
{
}

Location	*Map::operator[](const uint_fast8_t & height)
{
	return	(&_map[height * WIDTH]);
}
