#include <Windows.h>
#include "SFML/Graphics.hpp"
#include "Game.hh"

/*
int CALLBACK WinMain
(
	HINSTANCE   hInstance,
	HINSTANCE   hPrevInstance,
	LPSTR       lpCmdLine,
	int         nCmdShow
	)
{
	Gomoku	game;

	game.Run();
	return 0;
}
*/
int main()
{
	Gomoku	game;

	game.Run();
	return 0;

}