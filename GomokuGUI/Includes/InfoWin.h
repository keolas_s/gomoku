#pragma once

#include "SFML/Graphics.hpp"
#include "TGUI\TGUI.hpp"

class InfoWin
{
public:
	InfoWin(int, int, float, float);
	~InfoWin();
	void draw();
	void switchPlayer();
	void changeError(int i);

private:
	sf::RenderWindow _window;
	tgui::Gui guiinfo;
	void loadWidgets(tgui::Gui& gui);

};

InfoWin::InfoWin(int width, int height, float pos_x, float pos_y) :
_window(sf::VideoMode(width, height, 32), "", sf::Style::Close)
{
	_window.setPosition(sf::Vector2i(pos_x + width * 5 + 15, pos_y));
	
	guiinfo.setWindow(_window);
	guiinfo.setGlobalFont("arial.ttf");
	loadWidgets(guiinfo);
	
}

InfoWin::~InfoWin()
{

}

void InfoWin::loadWidgets(tgui::Gui &gui)
{

	tgui::Picture::Ptr background(guiinfo);
	background->load("./Ressources/kaya_background.jpg");
	background->scale(sf::Vector2f(2, 2));

	tgui::Picture::Ptr arrow1(guiinfo, "arrow");
	arrow1->load("./Ressources/arrow.png");
	arrow1->scale(sf::Vector2f(0.3f, 0.3f));
	arrow1->setPosition(_window.getSize().x / 15, 4);

	tgui::Label::Ptr player1(guiinfo, "player1");
	player1->setText("Player 1");
	player1->setPosition(_window.getSize().x / 4, 10);
	player1->setTextColor(sf::Color::White);
	player1->setTextSize(20);

	tgui::Label::Ptr player2(guiinfo, "player2");
	player2->setText("Player 2");
	player2->setPosition(_window.getSize().x / 4, 10 * 5);
	player2->setTextColor(sf::Color::Black);
	player2->setTextSize(20);

	tgui::Label::Ptr error(guiinfo, "error");
	error->setText("");
	error->setPosition(5, 10 * 70);
	error->setTextColor(sf::Color::Red);
	error->setTextSize(12);
	
}

void InfoWin::changeError(int i)
{
	tgui::Label::Ptr error = static_cast<tgui::Label::Ptr>(guiinfo.get("error"));

	if (i == 0)
		error->setText("");
	else if (i == 1)
		error->setText("Error : Breaking Five !");
	else
		error->setText("Error : Double Three !");
}

void InfoWin::switchPlayer()
{
	tgui::Label::Ptr player1 = static_cast<tgui::Label::Ptr>(guiinfo.get("player1"));
	tgui::Label::Ptr player2 = static_cast<tgui::Label::Ptr>(guiinfo.get("player2"));
	tgui::Picture::Ptr arrow = static_cast<tgui::Picture::Ptr>(guiinfo.get("arrow"));

	if (player1->getTextColor() == sf::Color::Black)
	{
		arrow->setPosition(arrow->getPosition().x, arrow->getPosition().y - 40);
		player1->setTextColor(sf::Color::White);
		player2->setTextColor(sf::Color::Black);
	}
	else
	{
		arrow->setPosition(arrow->getPosition().x, arrow->getPosition().y + 40);
		player1->setTextColor(sf::Color::Black);
		player2->setTextColor(sf::Color::White);
	}
}

void InfoWin::draw()
{
	_window.clear();
	guiinfo.draw();
	_window.display();
}
