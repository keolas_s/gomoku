#pragma once

class IMinMaxObject
{
public:
//	IMinMaxObject();
	virtual ~IMinMaxObject();

	virtual int	Eval() = 0;
	virtual bool	End() const = 0;
	virtual IMinMaxObject *	CreateCopy() = 0;
	virtual IMinMaxObject *	CreateTry() = 0;
	virtual Coor			getResult() = 0;
	virtual uint32_t		getCount() = 0;
private:

};
#pragma region Common
/*
IMinMaxObject::IMinMaxObject()
{
}
*/

IMinMaxObject::~IMinMaxObject()
{
}

#pragma endregion


