#pragma	once

#include <string>
#include "SFML/Graphics.hpp"


class Sprite
{
public:
	Sprite(const std::string & r_path = "Ressources/kaya_background.jpg");
	Sprite(const std::string & r_path , const sf::IntRect& rect);
	~Sprite();

	void	Draw(sf::RenderWindow & win);
	void	Scale(const sf::Vector2f&);
	void	setPos(const sf::Vector2f&);
	void	setColor(const sf::Color&);
private:
	sf::Texture *_texture;
	sf::Sprite	_sprite;
};

Sprite::Sprite(const std::string & r_path) :
_texture(new sf::Texture)
{
	if (_texture->loadFromFile(r_path) == false)
	{
	}
	_sprite.setTexture(*_texture);
}

Sprite::Sprite(const std::string & r_path, const sf::IntRect& rect) :
_texture(new sf::Texture)
{
	if (_texture->loadFromFile(r_path, rect) == false)
	{
	}
	_sprite.setTexture(*_texture);
}

Sprite::~Sprite()
{
}

void	Sprite::Draw(sf::RenderWindow & win)
{
	win.draw(this->_sprite);
}

void	Sprite::Scale(const sf::Vector2f& vector)
{
	_sprite.scale(vector);
}

void	Sprite::setPos(const sf::Vector2f& vector)
{
	_sprite.setPosition(vector);
}

void	Sprite::setColor(const sf::Color & color)
{
	_sprite.setColor(color);
}