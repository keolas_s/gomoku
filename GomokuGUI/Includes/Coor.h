#pragma once

#include <cstdint>

struct Coor
{
public:
	Coor(uint_fast8_t x, uint_fast8_t y) : x(x), y(y)
	{

	}
	uint_fast8_t	x;
	uint_fast8_t	y;
};