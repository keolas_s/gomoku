#pragma once

#include	"IMinMaxObject.hh"
#include	"Map.hh"
#include	"Arbitre.hh"

class Heuristic : public IMinMaxObject
{
public:
	Heuristic(const Map & map, const Arbitre & arbitre, const e_stone & color);
	Heuristic(const Heuristic& other);
	~Heuristic();

	void	setColor(const e_stone& color) { _color = color; }
	void	AddStone();

	virtual int	Eval();
	virtual bool	End() const;
	virtual IMinMaxObject *	CreateCopy();
	virtual IMinMaxObject *	CreateTry();
	virtual Coor			getResult();
	virtual uint32_t		getCount();

private:
	Map		_map;
	Arbitre	_arbitre;
	e_stone	_color;
	Coor	_stone;
};

Heuristic::Heuristic(const Map & map, const Arbitre & arbitre, const e_stone & color) :
_map(map),
_arbitre(arbitre),
_color(color),
_stone(0,0)
{

}

Heuristic::Heuristic(const Heuristic& other) :
_map(other._map),
_arbitre(other._arbitre),
_color(other._color),
_stone(0, 0)
{

}

Heuristic::~Heuristic()
{

}

#pragma region	MinMax
int	Heuristic::Eval()
{
	Location	temp;
	int			size;
	int			block;
	int			tscore = 0;
	int			temp_score = 0;


	for (uint_fast8_t y = 1; y < _map.HEIGHT; ++y)
	{
		for (uint_fast8_t x = 1; x < _map.WIDTH; ++x)
		{
			temp = _map.At(x, y);
			for (e_Direction i = N_S; i < END_DIR; ++i)
			{
				if (temp.getWay(i, IW_IS) == true)
				{
					size = temp.getWay(i, IW_SIZE);
					block = temp.getWay(i, IW_BLOCK);
					temp_score = 0;

					if (temp.GetType() == S_BLACK)
					{
						switch (size)
						{
						case 2:
							if (block == 0)
								temp_score += 2;
							else if (block == 1)
								temp_score += 1;
							break;
						case 3:
							if (block == 0)
								temp_score += 4;
							else if (block == 1)
								temp_score += 2;
							break;
						case 4:
							if (block == 0)
								temp_score += 8;
							else if (block == 1)
								temp_score += 4;

							break;
						case 5:
							if (block == 0)
								temp_score += 16;
							else if (block == 1)
								temp_score += 8;
							break;
						default:
							break;
						}
					}
					else if (S_WHITE)
					{
						switch (size)
						{
						case 2:
							if (block == 0)
								temp_score -= -16;
							else if (block == 1)
								temp_score += 8;
							else if (block == 2)
								temp_score += 16;
							break;
						case 3:
							if (block == 0)
								temp_score -= -32;
							else if (block == 1)
								temp_score += 16;
							else if (block == 2)
								temp_score += 32;
							break;
						case 4:
							if (block == 0)
								temp_score -= 64;
							else if (block == 1)
								temp_score += 32;
							else if (block == 2)
								temp_score += 64;

							break;
						case 5:
							if (block == 0)
								temp_score -= 128;
							else if (block == 1)
								temp_score += 8;
							break;
						default:
							break;
						}
					}
					tscore += temp_score;
				}

			}
		}
	}
	return (tscore);
}

bool	Heuristic::End() const
{
	return false;
}

IMinMaxObject *	Heuristic::CreateCopy()
{
	IMinMaxObject * ret = new Heuristic(*this);

	return	(ret);
}

void	Heuristic::AddStone()
{
	int_fast8_t		x = (rand() % (_map.WIDTH - 1)) + 1, y = (rand() % (_map.HEIGHT - 1)) + 1;

	while (_arbitre.BeforeRules(x, y, _color) == false || _map.getValid(x, y) == false)
	{
		x = (rand() % (_map.WIDTH - 1)) + 1;
		y = (rand() % (_map.HEIGHT - 1)) + 1;
	}
	_stone.x = x;
	_stone.y = y;
	_map.AddStone(x, y, _color);
}

IMinMaxObject *	Heuristic::CreateTry()
{
	Heuristic * temp = new Heuristic(*this);

	temp->setColor(!_color);
	temp->AddStone();

	return	temp;
}

Coor			Heuristic::getResult()
{
	return _stone;
}

uint32_t		Heuristic::getCount()
{
	return _map._count;
}


#pragma endregion
