#pragma once
#include "SFML/Graphics.hpp"
#include "TGUI\TGUI.hpp"

#define MAX_NUMBER_OF_ITEMS 4

class Menu
{
public:
	Menu(float width, float height, sf::RenderWindow &window);
	~Menu();

	void draw(sf::RenderWindow &window);
	void loadWidgets(tgui::Gui& gui);
	int callback();
	void handle(sf::Event &event);
	bool getRule1();
	bool getRule2();

private:
	int _width;
	int _height;
	int selectedItemIndex;
	sf::Font font;
	sf::Text menu[MAX_NUMBER_OF_ITEMS];
	tgui::Gui gui;

};


Menu::Menu(float width, float height, sf::RenderWindow &window) :
_width(width),
_height(height)
{
	gui.setWindow(window);
	gui.setGlobalFont("arial.ttf");
	loadWidgets(gui);
}

Menu::~Menu()
{

}

void Menu::draw(sf::RenderWindow &window)
{
	gui.draw();
}

void Menu::loadWidgets(tgui::Gui &gui)
{
	tgui::Button::Ptr but_playia(gui);
	but_playia->load("./Ressources/Black.conf");
	but_playia->setSize(200, 40);
	but_playia->setPosition(_width / 2 - 100, _height / (MAX_NUMBER_OF_ITEMS + 1) * 1);
	but_playia->setText("Play vs IA");
	but_playia->bindCallback(tgui::Button::LeftMouseClicked);
	but_playia->setCallbackId(1);

	tgui::Button::Ptr but_playhum(gui);
	but_playhum->load("./Ressources/Black.conf");
	but_playhum->setSize(200, 40);
	but_playhum->setPosition(_width / 2 - 100, _height / (MAX_NUMBER_OF_ITEMS + 1) * 2);
	but_playhum->setText("Play vs Human");
	but_playhum->bindCallback(tgui::Button::LeftMouseClicked);
	but_playhum->setCallbackId(2);

	tgui::Button::Ptr but_quit(gui);
	but_quit->load("./Ressources/Black.conf");
	but_quit->setSize(200, 40);
	but_quit->setPosition(_width / 2 - 100, _height / (MAX_NUMBER_OF_ITEMS + 1) * 3);
	but_quit->setText("Exit");
	but_quit->bindCallback(tgui::Button::LeftMouseClicked);
	but_quit->setCallbackId(3);

	tgui::Checkbox::Ptr checkRule1(gui, "breakingfive");
	checkRule1->load("./Ressources/Black.conf");
	checkRule1->setPosition(100, _height / (MAX_NUMBER_OF_ITEMS + 1) * 4);
	checkRule1->setText("Breaking Five");
	checkRule1->setTextColor(sf::Color::Black);
	checkRule1->check();

	tgui::Checkbox::Ptr checkRule2(gui, "doublethree");
	checkRule2->load("./Ressources/Black.conf");
	checkRule2->setPosition(500, _height / (MAX_NUMBER_OF_ITEMS + 1) * 4);
	checkRule2->setText("Double Three");
	checkRule2->setTextColor(sf::Color::Black);
	checkRule2->check();

}

void Menu::handle(sf::Event &event)
{
	gui.handleEvent(event);
}

bool Menu::getRule1()
{
	return (static_cast<tgui::Checkbox::Ptr>(gui.get("breakingfive")).get()->isChecked());
}

bool Menu::getRule2()
{
	return (static_cast<tgui::Checkbox::Ptr>(gui.get("doublethree")).get()->isChecked());
}

int Menu::callback()
{
	tgui::Callback call;
	while (gui.pollCallback(call))
	{	
		return (call.id);
	}
}
