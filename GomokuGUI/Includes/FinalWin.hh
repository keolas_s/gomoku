#pragma once

#include "SFML/Graphics.hpp"
#include "TGUI\TGUI.hpp"

class FinalWin
{
public:
	FinalWin();
	~FinalWin();
	void Init(sf::RenderWindow *window);
	void Draw();
	void loadWidgets();
	void changeWinner(int i);
	int	 callback();
	void handle(sf::Event &event);

private:
	sf::RenderWindow					*_window;
	tgui::Gui gui;

};

FinalWin::FinalWin()
{

}

FinalWin::~FinalWin()
{

}

void FinalWin::Init(sf::RenderWindow *_win)
{
	_window = _win;
	gui.setWindow(*_window);
	gui.setGlobalFont("arial.ttf");
	loadWidgets();
}

void FinalWin::Draw()
{
	gui.draw();
}

void FinalWin::loadWidgets()
{
	tgui::Label::Ptr winner(gui, "winner");
	winner->setText("Fin de la partie !");
	winner->setPosition(_window->getSize().x / 6, _window->getSize().y / 3);
	winner->setTextColor(sf::Color::White);
	winner->setTextSize(50);

	tgui::Button::Ptr but_quit(gui);
	but_quit->load("./Ressources/Black.conf");
	but_quit->setSize(300, 60);
	but_quit->setPosition(_window->getSize().x / 2 - 150, _window->getSize().y - 200);
	but_quit->setText("Exit");
	but_quit->bindCallback(tgui::Button::LeftMouseClicked);
	but_quit->setCallbackId(1);
}

void FinalWin::changeWinner(int i)
{
	tgui::Label::Ptr winner = static_cast<tgui::Label::Ptr>(gui.get("winner"));

	if (i == 0)
		winner.get()->setText("The winner is Player 1 !");
	else if (i == 1)
		winner.get()->setText("The winner is Player 2 !");
	else if (i == 3)
		winner.get()->setText("The winner is Player !");
	else
		winner.get()->setText("The winner is Computer !");
}

void FinalWin::handle(sf::Event &event)
{
	gui.handleEvent(event);
}

int FinalWin::callback()
{
	tgui::Callback call;
	while (gui.pollCallback(call))
	{
		return (call.id);
	}
}