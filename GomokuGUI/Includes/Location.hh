#pragma	once

#include <cstdint>
#include <vector>
#include <iostream>
#include <bitset>
#include "Ordinate.hh"
#include "Stone.hh"
	

enum e_index_way : uint_fast8_t
{
	IW_IS = 0,
	IW_SIZE = 1,
	IW_BLOCK = 2
};

struct Location
{
private:
	const static uint_fast8_t _index[][2];
	const static uint_fast8_t _index_way[][2];
public:
	const static uint_fast8_t SIZE_WAY;
public:
	enum e_index : uint_fast8_t
	{
		I_TYPE = 0,
		I_BLACK = 1,
		I_WHITE = 2,
		I_WAY = 3,
		I_VALID = 4
	};
public:
	Location();
	~Location();
	void		Set(const e_index& index, const uint_fast8_t& value);
	void		SetLink(e_stone const& stone, uint_fast8_t const& side);
	void		DelLink(e_stone const& stone, uint_fast8_t const& side);
	uint_fast8_t	GetLink(e_stone const& stone) const;
	void			setWay(const e_Direction &direction, const e_index_way& index, const uint_fast8_t & value);
	uint_fast8_t	getWay(const e_Direction &direction, const e_index_way& index);
	void			Print();

public:
	uint_fast8_t	GetType() const;
	uint_fast8_t	GetValid() const;
private:
	uint_fast8_t	Get(const e_index& index) const;
	uint_fast64_t	_container;	/* 64 */
};




#pragma region ByteSet

#pragma region Descriptif
/* Name | Index | Size */
/* ___________________ */
/* Type |     0 |    2 */
/* ___________________ */
/* Black|     2 |    8 */
/* ___________________ */
/* White|    10 |    8 */
/* ___________________ */
/* Way  |    18 |    20*/
/* ___________________ */
/* ******************* */
#pragma endregion
#pragma region Index
const	uint_fast8_t	Location::_index[][2] = {
		{  0, 2 },
		{  2, 8 },
		{ 10, 8 },
		{ 18, 20},
		{ 38, 1}
};
#pragma endregion
#pragma region Way
const uint_fast8_t		Location::SIZE_WAY = 5;
const	uint_fast8_t	Location::_index_way[][2] = {
		{ 0, 1 },
		{ 1, 2 },
		{ 3, 2 }
};
#pragma endregion

#pragma endregion

#pragma region Common

Location::Location() :
_container(0)
{

}
Location::~Location()
{

}
void				Location::Set(const e_index& index, const uint_fast8_t& value)
{
	for (uint_fast8_t i = 0; i < _index[index][1]; ++i)
	{
		if ((_container >> (_index[index][0] + i) & 1) != ((value >> i) & 1))
			_container ^= ((uint_fast64_t)1) << (i + _index[index][0]);
	}
}
uint_fast8_t		Location::Get(const e_index& index) const
{
	uint_fast8_t	temp = 0;
	uint_fast64_t	temp_index = _container >> _index[index][0];

	for (uint_fast8_t i = 0; i < _index[index][1]; ++i)
	{
		if ((temp_index & 1) != 0)
		{
			temp += ((uint_fast64_t)1) << i;
		}
		temp_index >>= 1;
	}	
	return temp;
}
void				Location::Print()
{
	std::bitset<64>	bitset = _container;

	std::cout << bitset << std::endl;
}

#pragma endregion

#pragma region Way

void	Location::setWay(const e_Direction &direction, const e_index_way& index, const uint_fast8_t &value)
{
	uint_fast8_t	way;

	for (uint_fast8_t i = 0; i < _index_way[index][1]; ++i)
	{
		if ((_container >> (_index[I_WAY][0] + (SIZE_WAY * direction) + _index_way[index][0] + i) & 1) != ((value >> i) & 1))
			_container ^= ((uint_fast64_t)1) << (_index[I_WAY][0] + (SIZE_WAY * direction) + _index_way[index][0] + i);
	}
}

uint_fast8_t Location::getWay(const e_Direction &direction, const e_index_way& index)
{
	uint_fast8_t	temp = 0;
	uint_fast8_t	size = _index_way[index][1];
	uint_fast64_t	temp_index = _container >> (_index[I_WAY][0] + (SIZE_WAY * direction)  + _index_way[index][0]);

	for (uint_fast8_t i = 0; i < size; ++i)
	{
		if ((temp_index & 1) != 0)
		{
			temp += 1 << i;
		}
		temp_index >>= 1;
	}
	return temp;
}

#pragma endregion

#pragma region Link

void		Location::SetLink(e_stone const& stone, uint_fast8_t const& side)
{
	e_stone	temp;

	if (stone == S_OPP)
		temp = static_cast<e_stone>(Get(I_TYPE));
	else
		temp = stone;
	/* MODIFY THIS IF INDEX ENUM IS MODIFIED !!! */
	Set(static_cast<e_index>(temp), Get(static_cast<e_index>(temp)) ^ (1 << side));
}
void		Location::DelLink(e_stone const& stone, uint_fast8_t const& side)
{
	SetLink(stone, side);
}
uint_fast8_t	Location::GetLink(const e_stone& stone) const
{
	if (stone == S_WHITE || stone == S_BLACK)
	{
		return	Get(static_cast<e_index>(stone));
	}
	else
	{
		return 0;
	}
}

#pragma endregion

#pragma region Type

uint_fast8_t		Location::GetType() const
{
	return (this->Get(I_TYPE));
}


uint_fast8_t		Location::GetValid() const
{
	return (this->Get(I_VALID));
}

#pragma endregion Very Lonely

