#pragma	once

#include <deque>
#include "Coor.h"
#include "Location.hh"
#include "Cardinal.hh"
#include "IMinMaxObject.hh"

class Map
{
public:
	static const	uint_fast8_t HEIGHT = 21;
	static const	uint_fast8_t WIDTH = 21;

public:
	Map();
	Map(const Map& map);
	virtual ~Map();

	void		AddStone(const uint_fast8_t & x, const uint_fast8_t & y, const e_stone& stone);
	void		AddLink(const uint_fast8_t & x, const uint_fast8_t & y, const e_stone& stone);
	void		AddWay(const uint_fast8_t & x, const uint_fast8_t & y, const e_stone& stone);
	void		DelWay(const uint_fast8_t & x, const uint_fast8_t & y, const e_stone& stone);
	void		DelStone(const uint_fast8_t & x, const uint_fast8_t & y);

	
	Location	&At(const uint_fast8_t & height, const uint_fast8_t & width);
	Location	*operator[](const uint_fast8_t & height);
	bool		getValid(const uint_fast8_t & x, const uint_fast8_t & y);
	uint_fast8_t	getType(const uint_fast8_t & x, const uint_fast8_t & y);
	uint_fast8_t	getLink(const uint_fast8_t & x, const uint_fast8_t & y, const e_stone& stone);
	void			setWay(const uint_fast8_t & x, const uint_fast8_t & y, const e_Direction &direction, const e_index_way & index, const uint_fast8_t & value);
	uint_fast8_t	getWay(const uint_fast8_t & x, const uint_fast8_t & y, const e_Direction &direction, const e_index_way & index);
	void	searchFarestRow(const uint_fast8_t & x, const uint_fast8_t & y, e_Cardinal cardinal, const e_stone& stone, std::deque<Coor> & gap, std::deque<Coor> & ngap, bool & block);
	void	setValid(const uint_fast8_t & x, const uint_fast8_t & y, const bool& val);
	void		GrowView(const uint_fast8_t & x, const uint_fast8_t & y);

	uint32_t	_count;
	uint_fast8_t	_minx;
	uint_fast8_t	_maxx;
	uint_fast8_t	_miny;
	uint_fast8_t	_maxy;
private:
private:
	Location	*_map;
};
 

#pragma region Common
Map::Map() :
_map(new Location[HEIGHT * WIDTH]),
_count(0),
_minx(20),
_maxx(0),
_miny(20),
_maxy(0)
{

}

Map::Map(const Map& map) :
_map(new Location[HEIGHT * WIDTH]),
_count(map._count),
_minx(map._minx),
_maxx(map._maxx),
_miny(map._miny),
_maxy(map._maxy)
{
	memcpy(_map, map._map, HEIGHT * WIDTH * sizeof(Location));
}

Map::~Map()
{
	delete [] _map;
}

Location	*Map::operator[](const uint_fast8_t & height)
{
	return	(&_map[height * WIDTH]);
}

Location	&Map::At(const uint_fast8_t & height, const uint_fast8_t & width)
{
	return	(_map[(height * WIDTH) + width]);
}
#pragma endregion	

#pragma region Stone Mod

void		Map::AddStone(const uint_fast8_t & x, const uint_fast8_t & y, const e_stone& stone)
{
	At(x, y).Set(Location::I_TYPE, stone);
	AddLink(x, y, stone);
	AddWay(x, y, stone);
	GrowView(x, y);
	_count++;

}
void		Map::AddLink(const uint_fast8_t & x, const uint_fast8_t & y, const e_stone& stone)
{
	uint_fast8_t	nx, ny;

	for (e_Cardinal i = NORTH; i < END_COOR; ++i)
	{
		nx = x + _coor_mod[i][0];
		ny = y + _coor_mod[i][1];
		if (nx >= 0 && nx < Map::HEIGHT &&
			ny >= 0 && ny < Map::WIDTH)
		{
			At(nx, ny).SetLink(stone, !i);
		}
	}
}
void		Map::AddWay(const uint_fast8_t & x, const uint_fast8_t & y, const e_stone& stone)
{
	for (e_Cardinal i = NORTH; i < SOUTH; ++i)
	{
		std::deque<Coor>	gap1, ngap1, gap2, ngap2;
		bool				block1 = false, block2 = false;
		uint_fast8_t		size, nblock;
		uint_fast8_t		size1, size2;
		uint_fast8_t		nblock1 = 0, nblock2 = 0;

		searchFarestRow(x, y, i, stone, gap1, ngap1, block1);
		searchFarestRow(x, y, !i, stone, gap2, ngap2, block2);
		if (gap1.size() > 1 || gap2.size() > 1 ||
			(gap1.size() > 0 && gap1.size() == ngap1.size()) ||
			(gap2.size() > 0 && gap2.size() == ngap2.size()))
		{
			/*SIZE*/
			size1 = ngap1.size() + gap2.size();
			if (size1 > 5)
				size = 5;
			size2 = gap1.size() + ngap2.size();
			if (size2 > 5)
				size = 5;
			size = ((size1 > size2) ? (size1) : (size2));
			if (size > 5)
				size = 5;
			/*BLOCK*/
			if (block1 == true)
				++nblock1;
			if (block2 == true && ngap2.size() == gap2.size())
				++nblock1;
			if (block2 == true)
				++nblock2;
			if (block1 == true && ngap1.size() == gap1.size())
				++nblock2;
			nblock = ((size1 > size2) ? (nblock1) : (nblock2));
			/*   */
			/*SELF*/
			setWay(x, y, (e_Direction)i, IW_IS, true);
			setWay(x, y, (e_Direction)i, IW_SIZE, size - 1);
			setWay(x, y, (e_Direction)i, IW_BLOCK, nblock);
			/*EXPAND*/
			for (uint_fast8_t ii = 0; i < ngap1.size(); ++i)
			{
				uint_fast8_t		xx = ngap1.front().x, yy = ngap1.front().y;

				setWay(xx, yy, (e_Direction)i, IW_IS, true);
				setWay(xx, yy, (e_Direction)i, IW_SIZE, size - 1);
				setWay(xx, yy, (e_Direction)i, IW_BLOCK, nblock);
				ngap1.pop_front();
				gap1.pop_front();
			}
			for (uint_fast8_t ii = 0; i < ngap2.size(); ++i)
			{
				uint_fast8_t		xx = ngap2.front().x, yy = ngap2.front().y;

				setWay(xx, yy, (e_Direction)i, IW_IS, true);
				setWay(xx, yy, (e_Direction)i, IW_SIZE, size - 1);
				setWay(xx, yy, (e_Direction)i, IW_BLOCK, nblock);
				ngap2.pop_front();
				gap2.pop_front();
			}
			for (Coor c : gap1)
			{
				setWay(c.x, c.y, (e_Direction)i, IW_IS, true);
				setWay(c.x, c.y, (e_Direction)i, IW_SIZE, size1 - 1);
				setWay(c.x, c.y, (e_Direction)i, IW_BLOCK, nblock1);
			}
			for (Coor c : gap2)
			{
				setWay(c.x, c.y, (e_Direction)i, IW_IS, true);
				setWay(c.x, c.y, (e_Direction)i, IW_SIZE, size2 - 1);
				setWay(c.x, c.y, (e_Direction)i, IW_BLOCK, nblock2);
			}

		}
	}
}
void		Map::DelWay(const uint_fast8_t & x, const uint_fast8_t & y, const e_stone& stone)
{
	for (e_Cardinal i = NORTH; i < SOUTH; ++i)
	{
		std::deque<Coor>	gap1, ngap1, gap2, ngap2;
		bool				block1 = false, block2 = false;
		uint_fast8_t		size, nblock;
		uint_fast8_t		size1, size2;
		uint_fast8_t		nblock1 = 0, nblock2 = 0;

		setWay(x, y, (e_Direction)i, IW_IS, false);
		setWay(x, y, (e_Direction)i, IW_SIZE, 0);
		setWay(x, y, (e_Direction)i, IW_BLOCK, 0);
		searchFarestRow(x, y, i, stone, gap1, ngap1, block1);
		searchFarestRow(x, y, !i, stone, gap2, ngap2, block2);
		if (gap1.size() > 1 || gap2.size() > 1 ||
			(gap1.size() > 0 && gap1.size() == ngap1.size()) ||
			(gap2.size() > 0 && gap2.size() == ngap2.size()))
		{
			std::cout << "test" << std::endl;
			/*SIZE*/
			size1 = ngap1.size() + gap2.size();
			if (size1 > 5)
				size = 5;
			size2 = gap1.size() + ngap2.size();
			if (size2 > 5)
				size = 5;
			size = ((size1 > size2) ? (size1) : (size2));
			if (size > 5)
				size = 5;
			/*BLOCK*/
			if (block1 == true)
				++nblock1;
			if (block2 == true && ngap2.size() == gap2.size())
				++nblock1;
			if (block2 == true)
				++nblock2;
			if (block1 == true && ngap1.size() == gap1.size())
				++nblock2;
			nblock = ((size1 > size2) ? (nblock1) : (nblock2));
			/*   */
			/*SELF*/
			setWay(x, y, (e_Direction)i, IW_IS, true);
			setWay(x, y, (e_Direction)i, IW_SIZE, size - 1);
			setWay(x, y, (e_Direction)i, IW_BLOCK, nblock);
			/*EXPAND*/
			for (uint_fast8_t ii = 0; i < ngap1.size(); ++i)
			{
				uint_fast8_t		xx = ngap1.front().x, yy = ngap1.front().y;

				setWay(xx, yy, (e_Direction)i, IW_IS, true);
				setWay(xx, yy, (e_Direction)i, IW_SIZE, size - 1);
				setWay(xx, yy, (e_Direction)i, IW_BLOCK, nblock);
				ngap1.pop_front();
				gap1.pop_front();
			}
			for (uint_fast8_t ii = 0; i < ngap2.size(); ++i)
			{
				uint_fast8_t		xx = ngap2.front().x, yy = ngap2.front().y;

				setWay(xx, yy, (e_Direction)i, IW_IS, true);
				setWay(xx, yy, (e_Direction)i, IW_SIZE, size - 1);
				setWay(xx, yy, (e_Direction)i, IW_BLOCK, nblock);
				ngap2.pop_front();
				gap2.pop_front();
			}
			for (Coor c : gap1)
			{
				setWay(c.x, c.y, (e_Direction)i, IW_IS, true);
				setWay(c.x, c.y, (e_Direction)i, IW_SIZE, size1 - 1);
				setWay(c.x, c.y, (e_Direction)i, IW_BLOCK, nblock1);
			}
			for (Coor c : gap2)
			{
				setWay(c.x, c.y, (e_Direction)i, IW_IS, true);
				setWay(c.x, c.y, (e_Direction)i, IW_SIZE, size2 - 1);
				setWay(c.x, c.y, (e_Direction)i, IW_BLOCK, nblock2);
			}

		}
	}
}
void		Map::searchFarestRow(const uint_fast8_t & x, const uint_fast8_t & y, e_Cardinal cardinal, const e_stone& stone, std::deque<Coor> & gap, std::deque<Coor> & ngap, bool	& block)
{
	bool			end = false;
	bool			is_gap = false;
	uint_fast8_t	nx, ny;

	nx = x + _coor_mod[cardinal][0];
	ny = y + _coor_mod[cardinal][1];
	while (end == false && nx >= 0 && nx < Map::HEIGHT && ny >= 0 && ny < Map::WIDTH)
	{
		if ((getType(nx, ny) == stone || getType(nx, ny) == S_OPP) && is_gap == false)
		{
			gap.push_back(Coor(nx, ny));
			ngap.push_back(Coor(nx, ny));
		}
		else if ((getType(nx, ny) == stone || getType(nx, ny) == S_OPP) && is_gap == true)
		{
			gap.push_back(Coor(nx, ny));
		}
		else if (getType(nx, ny) == S_EMPTY && is_gap == false)
		{
			is_gap = true;
			gap.push_back(Coor(nx, ny));
		}
		else if (getType(nx, ny) == S_EMPTY && is_gap == true)
		{
			return;
		}
		else
		{
			end = true;
		}
		nx += _coor_mod[cardinal][0];
		ny += _coor_mod[cardinal][1];
	}
	block = true;
}
void		Map::DelStone(const uint_fast8_t & x, const uint_fast8_t & y)
{
	uint_fast8_t	nx, ny;
	e_stone			stone = static_cast<e_stone>(At(x, y).GetType());

	At(x, y).Set(Location::I_TYPE, S_EMPTY);
	for (e_Cardinal i = NORTH; i < END_COOR; ++i)
	{
		nx = x + _coor_mod[i][0];
		ny = y + _coor_mod[i][1];
		if (i < SOUTH)
		{
			setWay(x, y, (e_Direction)i, IW_IS, false);
			setWay(x, y, (e_Direction)i, IW_SIZE, 0);
			setWay(x, y, (e_Direction)i, IW_BLOCK, 0);
		}
		if (nx >= 0 && nx < Map::HEIGHT &&
			ny >= 0 && ny < Map::WIDTH)
		{
			At(nx, ny).DelLink(stone, !i);
			if (getType(nx, ny) != S_EMPTY)
				DelWay(nx, ny, (e_stone)(getType(nx, ny)));
		}
	}
}
void		Map::GrowView(const uint_fast8_t & x, const uint_fast8_t & y)
{

	for (int_fast8_t ny = ((y - 1 >= 0) ? (y - 1) : (0)); ny < HEIGHT && ny < y + 2; ++ny)
	{
		for (int_fast8_t nx = ((x - 1 >= 0) ? (x - 1) : (0)); nx < WIDTH && nx < x + 2; ++nx)
		{
			setValid(nx, ny, true);
		}
	}
}

#pragma endregion

#pragma region Get*

uint_fast8_t	Map::getType(const uint_fast8_t & x, const uint_fast8_t & y) 
{
	return (At(x, y).GetType());
}
uint_fast8_t	Map::getLink(const uint_fast8_t & x, const uint_fast8_t & y, const e_stone& stone)
{
	return (At(x, y).GetLink(stone));
}

bool	Map::getValid(const uint_fast8_t & x, const uint_fast8_t & y)
{

	return (At(x, y).GetValid());
}


#pragma endregion Get*

#pragma region Set*

void	Map::setValid(const uint_fast8_t & x, const uint_fast8_t & y, const bool& val)
{
	if (val == true)
		At(x, y).Set(Location::I_VALID, 1);
	else
		At(x, y).Set(Location::I_VALID, 0);
}
#pragma endregion

#pragma region Way

uint_fast8_t	Map::getWay(const uint_fast8_t & x, const uint_fast8_t & y, const e_Direction &direction, const e_index_way & index)
{
	return (At(x, y).getWay(direction, index));
}
void			Map::setWay(const uint_fast8_t & x, const uint_fast8_t & y, const e_Direction &direction, const e_index_way & index, const uint_fast8_t & value)
{
	At(x, y).setWay(direction, index, value);
}

#pragma endregion 

