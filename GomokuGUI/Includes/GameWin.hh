#pragma	once

#include <cstdint>
#include <string>
#include <map>
#include "Arbitre.hh"
#include "Sprite.hh"
#include "Map.hh"
#include "Menu.hh"
#include "InfoWin.h"
#include "FinalWin.hh"
#include "Heuristic.hh"
#include "MinMax.hh"


enum State
{
	MENU,
	PLAYIA,
	PLAYHUMAN,
	FINAL,
	QUIT
};

class GameWin
{
public:
#define __name "Gomoku"
	static const	uint16_t	_grid_scale = 20;
	static const	uint16_t	_atari_size = 4;

public:
	GameWin(Map &map, const uint16_t & width = 800 - 20, const uint16_t &height = 800 - 20);
	~GameWin();
	void								Run();
private:
	void								Draw();
	void								Init();
	void								DrawGrid();
	void								DrawStone();
	void								DrawCursor();
	void								RunMenu();
	void								RunGameHuman();
	void								RunGameIA();
	void								RunFinal();
	void								Update();
	void								checkCallback();
	void								checkFinalCall();

	void								setHeight(uint16_t);
	uint16_t							getHeigt() const;
	void								setWidth(uint16_t);
	uint16_t							getWidth() const;
	void								setCurX(uint16_t);
	uint16_t							getCurX() const;
	void								setCurY(uint16_t);
	uint16_t							getCurY() const;
	void								setSide(bool);
	bool								getSide() const;
private:
	Map									&_map;
	sf::RenderWindow					_window;
	uint16_t							_height;
	uint16_t							_width;
	uint16_t							_cur_x;
	uint16_t							_cur_y;
	bool								_side;
	sf::Event							_event;
	std::map<std::string, Sprite>		_sprites;
	Arbitre								_arbitre;
	Menu								_menu;
	State								_state;
	InfoWin								_infowin;
	FinalWin							_finalwin;
};


GameWin::GameWin(Map &map, const uint16_t & width, const uint16_t &height) :
_window(sf::VideoMode(width, height, 32), __name, sf::Style::Close),
_height(height),
_width(width),
_side(true),
_map(map),
_menu(width, height, _window),
_state(MENU),
_arbitre(&map, true, true),
_infowin(_width / 5, _height, _window.getPosition().x, _window.getPosition().y),
_finalwin()
{
	sf::RenderWindow *win = &_window;
	_finalwin.Init(win);
	Init();
}

void	GameWin::Init()
{
	_sprites.emplace(std::make_pair("Background", Sprite("./Ressources/kaya_background.jpg", sf::IntRect(0, 0, 400, 400))));
	_sprites["Background"].Scale(sf::Vector2f(2, 2));
	_sprites.emplace(std::make_pair("Stone", Sprite("./Ressources/pierre_blanc.png")));
	_sprites.emplace(std::make_pair("Stone Black", Sprite("./Ressources/pierre_noir.png")));

	srand(1566597); /* #Queenie */
}

void	GameWin::Draw()
{
	_window.clear();
	_sprites["Background"].Draw(_window);
	DrawGrid();
	DrawStone();
	DrawCursor();
	_window.display();
	_infowin.draw();
}

void	GameWin::RunMenu()
{
	_menu.handle(_event);
	
	
	switch (_event.type)
	{
	case sf::Event::Closed:
		_window.close();
		break;
	}
	
}

void	GameWin::RunFinal()
{
	_finalwin.handle(_event);
	switch (_event.type)
	{
	case sf::Event::Closed:
		_window.close();
		break;
	}
}

void	GameWin::RunGameHuman()
{
	if (_event.type == sf::Event::Closed)
		_window.close();
	else if (_event.type == sf::Event::MouseMoved)
	{
		_cur_x = _event.mouseMove.x;
		_cur_y = _event.mouseMove.y;
	}
	else if (_event.type == sf::Event::MouseButtonReleased)
	{
		uint16_t	w_scale = _width / _grid_scale;
		uint16_t	h_scale = _height / _grid_scale;
		uint16_t	cur_x = _cur_x + 25;
		uint16_t	cur_y = _cur_y + 25;

		_infowin.changeError(0);

		//Tu peux modifier l'error en passant en parametre 1 ==> Breaking Five / 2 ==> Double Three

		if ((cur_x > 40 && cur_y > 40) && (cur_x < getWidth() && cur_y < getHeigt()))
		{


			if (_arbitre.BeforeRules(cur_x / w_scale, cur_y / h_scale, ((_side == true) ? (S_WHITE) : (S_BLACK))) == true)
			{
				_map.AddStone((cur_x / w_scale), (cur_y / h_scale), ((_side == true) ? (S_WHITE) : (S_BLACK)));
				if (_arbitre.AfterRules(cur_x / w_scale, cur_y / h_scale, ((_side == true) ? (S_WHITE) : (S_BLACK))) == false)
				{
					std::cout << "END GAME" << std::endl;
					// CALL LE VAINQUEUR
					// PLAYER 1 ==> _finalwin.changeWinner(0);
					// PLAYER 2 ==> _finalwin.changewinner(1);
					_state = FINAL;
				}
				if (_side == true)
					_side = false;
				else
					_side = true;
				_infowin.switchPlayer();
			}
		}
	}
	else if (_event.key.code == sf::Keyboard::Space)
	{
		uint16_t	w_scale = _width / _grid_scale;
		uint16_t	h_scale = _height / _grid_scale;
		uint16_t	cur_x = _cur_x + 25;
		uint16_t	cur_y = _cur_y + 25;
		if ((cur_x > 40 && cur_y > 40) && (cur_x < getWidth() && cur_y < getHeigt()))
				_map.At((cur_x / w_scale), (cur_y / h_scale)).Print();
	}

}

void	GameWin::RunGameIA()
{

	if (_event.type == sf::Event::Closed)
		_window.close();
	else if (_event.type == sf::Event::MouseMoved)
	{
		_cur_x = _event.mouseMove.x;
		_cur_y = _event.mouseMove.y;
	}
	else if (_event.type == sf::Event::MouseButtonReleased)
	{
		uint16_t	w_scale = _width / _grid_scale;
		uint16_t	h_scale = _height / _grid_scale;
		uint16_t	cur_x = _cur_x + 25;
		uint16_t	cur_y = _cur_y + 25;
		if ((cur_x > 40 && cur_y > 40) && (cur_x < getWidth() && cur_y < getHeigt()))
		{

			if (_arbitre.BeforeRules(cur_x / w_scale, cur_y / h_scale, ((_side == true) ? (S_WHITE) : (S_BLACK))) == true)
			{
				_map.AddStone((cur_x / w_scale), (cur_y / h_scale), ((_side == true) ? (S_WHITE) : (S_BLACK)));
				if (_arbitre.AfterRules(cur_x / w_scale, cur_y / h_scale, ((_side == true) ? (S_WHITE) : (S_BLACK))) == false)
				{
					std::cout << "END GAME" << std::endl;
					_state = FINAL;
				}
				/*
				** LE TOUR DE L'IA !
				*/
				else
				{
					Heuristic	h(_map, _arbitre, ((_side == true) ? (S_BLACK) : (S_WHITE)));
					MinMax		m;
					Coor		c(0, 0);

					c = m.Start(&h);
					_map.AddStone(c.x, c.y, ((_side == true) ? (S_BLACK) : (S_WHITE)));
					if (_arbitre.AfterRules(c.x, c.y, ((_side == true) ? (S_BLACK) : (S_WHITE))) == false)
					{
						std::cout << "END GAME" << std::endl;
						_state = FINAL;
					}
				}
				_infowin.switchPlayer();
				_infowin.switchPlayer();
			}
		}
	}
	else if (_event.key.code == sf::Keyboard::Space)
	{
		uint16_t	w_scale = _width / _grid_scale;
		uint16_t	h_scale = _height / _grid_scale;
		uint16_t	cur_x = _cur_x + 25;
		uint16_t	cur_y = _cur_y + 25;
		if ((cur_x > 40 && cur_y > 40) && (cur_x < getWidth() && cur_y < getHeigt()))
			_map.At((cur_x / w_scale), (cur_y / h_scale)).Print();
	}

}

void	GameWin::DrawGrid()
{
	sf::Vertex	line(sf::Vector2f(0, 1));
	float		w_scale = _width / _grid_scale;
	float		h_scale = _width / _grid_scale;

	for (uint16_t i = 0; i < _grid_scale; i++)
	{
		sf::Vertex	line[] =
		{
			sf::Vertex(sf::Vector2f(w_scale * i, 0), sf::Color::Black),
			sf::Vertex(sf::Vector2f(w_scale * i, _height), sf::Color::Black)
		};

		_window.draw(line, 2, sf::Lines);
	}
	for (uint16_t i = 0; i < _grid_scale; i++)
	{
		sf::Vertex	line[] =
		{
			sf::Vertex(sf::Vector2f(0, h_scale * i), sf::Color::Black),
			sf::Vertex(sf::Vector2f(_height, h_scale * i), sf::Color::Black)
		};

		_window.draw(line, 2, sf::Lines);
	}

	sf::CircleShape	circle(_atari_size);
	for (uint16_t i = 3; i < _grid_scale; i += 6)
	{
		for (uint16_t j = 3; j < _grid_scale; j += 6)
		{
			circle.setPosition(sf::Vector2f(i * w_scale - _atari_size + 40, j * h_scale - _atari_size + 40));
			circle.setFillColor(sf::Color::Black);
			_window.draw(circle);
		}
	}
}

void	GameWin::DrawStone()
{
	float		w_scale = _width / _grid_scale;
	float		h_scale = _height/ _grid_scale;

	for (uint16_t i = 0; i <= _grid_scale; i++)
	{
		for (uint16_t j = 0; j <= _grid_scale; j++)
		{
			if (_map[i][j].GetType() == S_WHITE)
			{
				_sprites["Stone"].setPos(sf::Vector2f(i * w_scale - 25, j * h_scale - 25));
				_sprites["Stone"].Draw(_window);
			}
			else if (_map[i][j].GetType() == S_BLACK)
			{
				_sprites["Stone Black"].setPos(sf::Vector2f(i * w_scale - 25, j * h_scale - 25));
				_sprites["Stone Black"].Draw(_window);
			}
		}
	}
}

void	GameWin::DrawCursor()
{

	uint16_t	w_scale = _width / _grid_scale;
	uint16_t	h_scale = _height / _grid_scale;
	uint16_t	cur_x = _cur_x + 25;
	uint16_t	cur_y = _cur_y + 25;
	if ((cur_x > 40 && cur_y > 40) && (cur_x < getWidth() && cur_y < getHeigt()))
	{
		if (_side == true)
		{
			_sprites["Stone"].setPos(sf::Vector2f(cur_x - (cur_x % (int)w_scale) - 25, cur_y - (cur_y % (int)h_scale) - 25));
			_sprites["Stone"].Draw(_window);
		}
		else
		{
			_sprites["Stone Black"].setPos(sf::Vector2f(cur_x - (cur_x % (int)w_scale) - 25, cur_y - (cur_y % (int)h_scale) - 25));
			_sprites["Stone Black"].Draw(_window);
		}
	}
}

GameWin::~GameWin()
{
}

void	GameWin::checkCallback()
{
	int nb = _menu.callback();

	if (nb == 1)
		_state = PLAYIA;
	else if (nb == 2)
		_state = PLAYHUMAN;
	else if (nb == 3)
		_state = QUIT;
}

void	GameWin::checkFinalCall()
{
	int nb = _finalwin.callback();
	if (nb == 1)
		_state = QUIT;
}

void	GameWin::Update()
{
	switch (_state)
	{
	case MENU:
		_window.clear();
		_sprites["Background"].Draw(_window);
		_menu.draw(_window);
		_window.display();
		checkCallback();
		break;
	case PLAYHUMAN:
		Draw();
		break;
	case PLAYIA:
		Draw();
		break;
	case FINAL:
		_window.clear();
		_sprites["Background"].Draw(_window);
		_finalwin.Draw();
		_window.display();
		checkFinalCall();
		break;
	case QUIT:
		break;
	default:
		break;
	}
}

void	GameWin::Run()
{

	while (_window.isOpen())
	{
		while (_window.pollEvent(_event))
		{
			switch (_state)
			{
			case MENU:
				RunMenu();
				break;
			case PLAYIA:
				RunGameIA();
				break;
			case PLAYHUMAN:
				RunGameHuman();
				break;
			case FINAL:
				RunFinal();
				break;
			case QUIT:
				_window.close();
				break;
			default:
				break;
			}
		}
		Update();
	}
}

/*
** SETTERS & GETTERS
*/

void	GameWin::setHeight(uint16_t height)
{
	this->_height = height;
}

uint16_t	GameWin::getHeigt() const
{
	return this->_height;
}

void	GameWin::setWidth(uint16_t width)
{
	this->_width = width;
}

uint16_t	GameWin::getWidth() const
{
	return (this->_width);
}

void	GameWin::setCurX(uint16_t x)
{
	this->_cur_x = x;
}

uint16_t	GameWin::getCurX() const
{
	return this->_cur_x;
}

void	GameWin::setCurY(uint16_t y)
{
	this->_cur_y = y;
}

uint16_t	GameWin::getCurY() const
{
	return (this->_cur_y);
}

void	GameWin::setSide(bool side)
{
	this->_side = side;
}

bool	GameWin::getSide() const
{
	return (this->_side);
}