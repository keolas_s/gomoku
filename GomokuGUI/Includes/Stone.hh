#pragma once

#include	<cstdint>

enum e_stone : int_fast8_t
{
	S_EMPTY = 0,
	S_WHITE = 1,
	S_BLACK = 2,
	S_OPP = 3
};

e_stone	operator!(e_stone & other)
{
	if (other == S_WHITE)
		return	S_BLACK;
	else if (other == S_BLACK)
		return	S_WHITE;
	else
		return	S_EMPTY;
}
