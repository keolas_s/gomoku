#pragma	once

#include	"GameWin.hh"

class Gomoku
{
public:
	Gomoku();
	~Gomoku();
	void	Run();
private:
	Map		_map;
	GameWin	_GW;
};

Gomoku::Gomoku():
_GW(_map)
{

}

Gomoku::~Gomoku()
{
}

void	Gomoku::Run()
{
	_GW.Run();
}
