#pragma	once

#include <cstdint>

enum e_Cardinal : uint_fast8_t
{
	NORTH = 0,
	NORTH_EAST = 1,
	EAST = 2,
	SOUTH_EAST = 3,
	SOUTH = 4,
	SOUTH_WEST = 5,
	WEST = 6,
	NORTH_WEST = 7,
	END_COOR = 8
};

enum e_Direction : uint_fast8_t
{
	N_S = 0,
	NE_SW = 1,
	E_W = 2,
	SE_NW = 3,
	END_DIR = 4
};

e_Cardinal & operator++(e_Cardinal& other)
{
	other = static_cast<e_Cardinal>(((static_cast<uint_fast8_t>(other)) + 1) % 9);
	return other;
}

e_Cardinal operator!(e_Cardinal & other)
{
	return static_cast<e_Cardinal>(((static_cast<uint_fast8_t>(other)) + 4) % 8);
}

e_Direction & operator++(e_Direction& other)
{
	other = static_cast<e_Direction>(((static_cast<uint_fast8_t>(other)) + 1) % 5);
	return other;
}

const int_fast8_t _coor_mod[][2] = {
		{ -1, 0 },			//NORTH
		{ -1, 1 },			//NORTH_EAST
		{ 0, 1 },			//EAST
		{ 1, 1 },			//SOUTH_EAST
		{ 1, 0 },			//SOUTH
		{ 1, -1 },			//SOUTH_WEST
		{ 0, -1 },			//WEST
		{ -1, -1 }			//NORTH_WEST
};