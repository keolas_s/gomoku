#pragma	once

#include	"Cardinal.hh"
#include	"Location.hh"
#include	"Map.hh"
#include	"Stone.hh"

class Arbitre
{
public:
	Arbitre(Map * map, const bool & rule1 = true, const bool & rule2 = true);
	Arbitre(const Arbitre& other);
	~Arbitre();
	
	/*SETTEUR*/
	void	setRuleBreakFive(const bool & rule) { _rule2 = rule; }
	void	setRuleDoubleThree(const bool & rule) { _rule1 = rule; }

	/**/
	bool    BeforeRules(const uint_fast8_t & x, const uint_fast8_t & y, const e_stone& stone);
	bool    AfterRules(const uint_fast8_t & x, const uint_fast8_t & y, const e_stone& stone);
	bool    EndRules() const;
	/* RULES */
	bool	RuleFull(const uint_fast8_t & x, const uint_fast8_t & y);
	bool	RuleTake(const uint_fast8_t & x, const uint_fast8_t & y, e_stone stone);
	bool	RuleDoubleThree(const uint_fast8_t & x, const uint_fast8_t & y, e_stone stone);
	bool	RuleFive(const uint_fast8_t & x, const uint_fast8_t & y, e_stone stone);
	bool	RuleBreakFive(std::deque<Coor> queue, e_stone stone, const e_Cardinal & side);
	/* ***** */
private:
	bool	recCheck(
		const uint_fast8_t & x,
		const uint_fast8_t & y,
		const e_stone& stone,
		const uint_fast8_t& count,
		const e_Cardinal & side)const;/* RuleTake */
	uint_fast8_t	rdt_with(
		const uint_fast8_t & x,
		const uint_fast8_t & y,
		const e_stone& stone,
		const e_Cardinal & side,
		const uint_fast8_t& free)const; /* RuleDoubleThree */
	uint_fast8_t	rdt_without(
		const uint_fast8_t & x,
		const uint_fast8_t & y,
		const e_stone& stone,
		const e_Cardinal & side) const; /* RuleDoubleThree */
	bool	rdt_second_with(
		const uint_fast8_t & x,
		const uint_fast8_t & y,
		const e_stone& stone,
		const e_Cardinal & side,
		const uint_fast8_t& free) const; /* RuleDoubleThree */
	bool	rdt_second_without(
		const uint_fast8_t & x,
		const uint_fast8_t & y,
		const e_stone& stone,
		const e_Cardinal & side) const; /* RuleDoubleThree */	
	bool	rdt_second_block(
		const uint_fast8_t & x,
		const uint_fast8_t & y,
		e_stone stone,
		const e_Cardinal & block) const; /* RuleDoubleThree */
	bool	rdt_new_check(const uint_fast8_t & x, const uint_fast8_t & y, e_stone stone); /* RuleDoubleThree */



//	uint_fast8_t	& adjustX(const uint_fast8_t & x, e_ordinate ordinate) const;
private:
public:
	Map							*_map;
	uint_fast8_t				_height;
	uint_fast8_t				_width;
	bool						_rule1;
	bool						_rule2;

	int_fast8_t					_rbf_last_turn;
	std::deque<Coor>			_last_turn;
	e_stone						_last_stone;
	e_Cardinal					_last_block;

	bool						_end;
};



Arbitre::Arbitre(Map * map, const bool & rule1, const bool & rule2) :
_map(map),
_rule1(rule1),
_rule2(rule2),
_rbf_last_turn(0),
_end(false)
{

}

Arbitre::Arbitre(const Arbitre& other) :
_map(new Map(*(other._map))),
_rule1(other._rule1),
_rule2(other._rule2),
_rbf_last_turn(other._rbf_last_turn),
_end(other._end)
{

}

Arbitre::~Arbitre()
{
}

bool    Arbitre::BeforeRules(const uint_fast8_t & x, const uint_fast8_t & y, const e_stone& stone)
{
	if (RuleFull(x, y) == false ||
		(_rule1 == true && RuleDoubleThree(x, y, stone) == false))
		return	false;
	else
	{
		return true;
	}
}

bool    Arbitre::AfterRules(const uint_fast8_t & x, const uint_fast8_t & y, const e_stone& stone)
{

	--_rbf_last_turn;
	if (RuleTake(x, y, stone) == false)
	{
		return true;
	}
	else if (RuleFive(x, y, stone) == false/* Plus Breaking Five */)
	{
		_end = true;
		return	false;
	}
	else
	{
		return true;
	}
	return true;
}

bool    Arbitre::EndRules() const 
{
	return _end;
}


bool	Arbitre::RuleFull(const uint_fast8_t & x, const uint_fast8_t & y)
{
	if (_map->At(x, y).GetType() == S_EMPTY)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool	Arbitre::RuleTake(const uint_fast8_t & x, const uint_fast8_t & y, e_stone stone)
{
	uint_fast8_t	nx, ny, mask_ally, mask_opp;

	mask_ally = _map->At(x, y).GetLink(stone);
	mask_opp = _map->At(x, y).GetLink(!stone);

	for (e_Cardinal i = NORTH; i < END_COOR; ++i)
	{
		nx = x + _coor_mod[i][0];
		ny = y + _coor_mod[i][1];
		if (nx >= 0 && nx < Map::HEIGHT &&
			ny >= 0 && ny < Map::WIDTH &&
			((mask_opp >> i) & 1) == 1)
		{
			if (recCheck(x, y, !stone, 2, i) == true &&
				recCheck(x, y, stone, 3, i) == true)
			{
				_map->DelStone(x + _coor_mod[i][0], y + _coor_mod[i][1]);
				_map->DelStone(x + (_coor_mod[i][0] * 2), y + (_coor_mod[i][1] * 2));
			}
		}
	}
	return	true;
}

bool	Arbitre::recCheck(
	const uint_fast8_t & x,
	const uint_fast8_t & y,
	const e_stone& stone,
	const uint_fast8_t& count,
	const e_Cardinal & side) const
{
	uint_fast8_t	nx, ny;

	nx = x + _coor_mod[side][0] * count;
	ny = y + _coor_mod[side][1] * count;
	if (nx >= 0 && nx < Map::HEIGHT &&
		ny >= 0 && ny < Map::WIDTH)
	{
		if (_map->At(nx, ny).GetType() == stone)
			return	true;
	}
	return	false;
}

bool	Arbitre::RuleDoubleThree(const uint_fast8_t & x, const uint_fast8_t & y, e_stone stone)
{
	if (rdt_new_check(x, y, stone) == false)
		return (false);
	_map->At(x, y).Set(Location::I_TYPE, S_OPP);
	for (e_Cardinal i = NORTH; i < SOUTH; ++i)
	{

		std::deque<Coor>	gap1, ngap1, gap2, ngap2;
		bool				block1 = false, block2 = false;
		uint_fast8_t		size, nblock;
		uint_fast8_t		size1, size2;
		uint_fast8_t		nblock1 = 0, nblock2 = 0;

		_map->searchFarestRow(x, y, i, stone, gap1, ngap1, block1);
		_map->searchFarestRow(x, y, !i, stone, gap2, ngap2, block2);
		if (gap1.size() != 0 || gap2.size() != 0)
		{
			for (uint_fast8_t ii = 0; i < ngap1.size(); ++i)
			{
				if (rdt_new_check(ngap1.front().x, ngap1.front().y, stone) == false)
				{
					_map->At(x, y).Set(Location::I_TYPE, S_EMPTY);
					return (false);
				}
				ngap1.pop_front();
				gap1.pop_front();
			}
			for (uint_fast8_t ii = 0; i < ngap2.size(); ++i)
			{
				if (rdt_new_check(ngap2.front().x, ngap2.front().y, stone) == false)
				{
					_map->At(x, y).Set(Location::I_TYPE, S_EMPTY);
					return (false);
				}
				ngap2.pop_front();
				gap2.pop_front();
			}
			for (Coor c : gap1)
			{
				if (rdt_new_check(c.x, c.y, stone) == false)
				{
					_map->At(x, y).Set(Location::I_TYPE, S_EMPTY);
					return (false);
				}
			}
			for (Coor c : gap2)
			{
				if (rdt_new_check(c.x, c.y, stone) == false)
				{
					_map->At(x, y).Set(Location::I_TYPE, S_EMPTY);
					return (false);
				}
			}
		}
	}
	_map->At(x, y).Set(Location::I_TYPE, S_EMPTY);
	return	true;
}

bool	Arbitre::rdt_new_check(const uint_fast8_t & x, const uint_fast8_t & y, e_stone stone)
{
	uint_fast8_t	count = 0;

	for (e_Cardinal i = NORTH; i < SOUTH; ++i)
	{

		std::deque<Coor>	gap1, ngap1, gap2, ngap2;
		bool				block1 = false, block2 = false;
		uint_fast8_t		size, nblock;
		uint_fast8_t		size1, size2;
		uint_fast8_t		nblock1 = 0, nblock2 = 0;

		_map->searchFarestRow(x, y, i, stone, gap1, ngap1, block1);
		_map->searchFarestRow(x, y, !i, stone, gap2, ngap2, block2);
		if (gap1.size() != 0 || gap2.size() != 0)
		{
			/*SIZE*/
			size1 = ngap1.size() + gap2.size();
			size2 = gap1.size() + ngap2.size();
			size = ((size1 > size2) ? (size1) : (size2));
			if (block1 == true)
				++nblock1;
			if (block2 == true && ngap2.size() == gap2.size())
				++nblock1;
			if (block2 == true)
				++nblock2;
			if (block1 == true && ngap1.size() == gap1.size())
				++nblock2;
			if ((size1 >= 3 && block1 == 0) || (size2 >= 3 && block2 == 0))
				++count;
		}
	}
	if (count >= 2)
	{
//		std::cout << "Double 3 dans tes dents !" << std::endl;
		return false;
	}
	else
		return true;
}

			/* looking for the longest suite with 1 space */
uint_fast8_t	Arbitre::rdt_with(
	const uint_fast8_t & x,
	const uint_fast8_t & y,
	const e_stone& stone,
	const e_Cardinal & side,
	const uint_fast8_t& free) const
{
	uint_fast8_t	nx, ny;

	nx = x + _coor_mod[side][0];
	ny = y + _coor_mod[side][1];
	if (nx >= 0 && nx < Map::HEIGHT &&
		ny >= 0 && ny < Map::WIDTH)
	{
		if (_map->At(nx, ny).GetType() == stone)
			return (rdt_with(nx, ny, stone, side, free) + 1);
		else if (_map->At(nx, ny).GetType() == S_EMPTY)
		{
			if (free > 0)
				return (rdt_with(nx, ny, stone, side, free - 1));
			else
				return	0;
		}
		else
			return	0;
	}
	return	false;
}

			/* looking for the longest suite without space */
uint_fast8_t	Arbitre::rdt_without(
	const uint_fast8_t & x,
	const uint_fast8_t & y,
	const e_stone& stone,
	const e_Cardinal & side) const
{
	uint_fast8_t	nx, ny;

	nx = x + _coor_mod[side][0];
	ny = y + _coor_mod[side][1];
	if (nx >= 0 && nx < Map::HEIGHT &&
		ny >= 0 && ny < Map::WIDTH)
	{
		if (_map->At(nx, ny).GetType() == stone)
			return (rdt_without(nx, ny, stone, side) + 1);
		else
			return	0;
	}
	return	false;
}

			/* looking for the second three with space */
bool	Arbitre::rdt_second_with(
	const uint_fast8_t & x,
	const uint_fast8_t & y,
	const e_stone& stone,
	const e_Cardinal & side,
	const uint_fast8_t& free) const
{
	uint_fast8_t	nx, ny;

	nx = x + _coor_mod[side][0];
	ny = y + _coor_mod[side][1];
	if (nx >= 0 && nx < Map::HEIGHT &&
		ny >= 0 && ny < Map::WIDTH)
	{
		if (_map->At(nx, ny).GetType() == stone)
		{
			if (rdt_second_block(nx, ny, stone, side) == false)
				return false;
			return (rdt_second_with(nx, ny, stone, side, free));
		}
		else if (_map->At(nx, ny).GetType() == S_EMPTY)
		{
			if (free > 0)
				return (rdt_second_with(nx, ny, stone, side, free - 1));
			else
				return	true;
		}
		else
			return	true;
	}
	return	true;
}

bool	Arbitre::rdt_second_without(
	const uint_fast8_t & x,
	const uint_fast8_t & y,
	const e_stone& stone,
	const e_Cardinal & side) const
{
	uint_fast8_t	nx, ny;

	nx = x + _coor_mod[side][0];
	ny = y + _coor_mod[side][1];
	if (nx >= 0 && nx < Map::HEIGHT &&
		ny >= 0 && ny < Map::WIDTH)
	{
		if (_map->At(nx, ny).GetType() == stone)
		{
			if (rdt_second_block(nx, ny, stone, side) == false)
				return false;
			return (rdt_second_without(nx, ny, stone, side));
		}
		else
			return	true;
	}
	return	true;
}

bool	Arbitre::rdt_second_block(
	const uint_fast8_t & x,
	const uint_fast8_t & y,
	e_stone stone,
	const e_Cardinal & block) const
{
	uint_fast8_t	nx, ny, mask_opp, mask_ally;

	mask_ally = _map->At(x, y).GetLink(stone);
	mask_opp = _map->At(x, y).GetLink(!stone);
	for (e_Cardinal i = NORTH; i < END_COOR; ++i)
	{
		nx = x + _coor_mod[i][0];
		ny = y + _coor_mod[i][1];
		if (nx >= 0 && nx < Map::HEIGHT &&
ny >= 0 && ny < Map::WIDTH &&
	((mask_opp >> i) & 1) == 0)
{
	if (i != block && i != ((block + 4) % 8))
	{
		//				std::cout << "Block: " << (int)block << "/" << (int)((block + 4) % 8) << " : " << (int)i << std::endl;
		std::cout << "For " << (int)i << std::endl;
		std::cout << "Block: " << (int)(rdt_with(nx, ny, stone, i, 1) + (rdt_without(nx, ny, stone, !i))) << std::endl;
		//				std::cout << "Block: " << (int)i<< "/" << (int)(!i) << std::endl;
		if ((rdt_with(x, y, stone, i, 1)) + (rdt_without(x, y, stone, !i)) >= 2)
		{
			std::cout << "Double Three !!!!" << std::endl;
			std::cout << (int)block << std::endl;
			return false;
		}
	}
}
	}
	return true;
}


bool	Arbitre::RuleFive(const uint_fast8_t & x, const uint_fast8_t & y, e_stone stone)
{
	if (_rbf_last_turn >= 0 && RuleBreakFive(_last_turn, _last_stone, _last_block) == false)
		return false;
	for (e_Cardinal i = NORTH; i < SOUTH; ++i)
	{
		std::deque<Coor>	gap1, ngap1, gap2, ngap2;
		bool				block1 = false, block2 = false;
		uint_fast8_t		size, nblock;
		uint_fast8_t		size1, size2;
		uint_fast8_t		nblock1 = 0, nblock2 = 0;

		_map->searchFarestRow(x, y, i, stone, gap1, ngap1, block1);
		_map->searchFarestRow(x, y, !i, stone, gap2, ngap2, block2);
		if (ngap1.size() + ngap2.size() >= 4)
		{
			std::cout << "SUITE DE 5 !!!!" << std::endl;
			if (_rule2 == false)
				return false;
			else
			{
				for (Coor c : ngap2)
				{
					ngap1.push_back(c);
				}
				ngap1.push_back(Coor(x, y));
				/* ngap1 = list of elem */

				if (RuleBreakFive(ngap1, stone, i) == true)
					return false;
			}

		}
	}
	return true;
}

bool	Arbitre::RuleBreakFive(std::deque<Coor> queue, e_stone stone, const e_Cardinal & side)
{
	for (Coor c : queue)
	{
		for (e_Cardinal i = NORTH; i < END_COOR; ++i)
		{
			if (((i + 8) % 4) != side)
			{
				uint_fast8_t	nx, ny;

				nx = c.x + _coor_mod[i][0];
				ny = c.y + _coor_mod[i][1];
				if (nx >= 0 && nx < Map::HEIGHT && ny >= 0 && ny < Map::WIDTH &&
					_map->getType(nx, ny) == stone)
				{
					nx = c.x + (_coor_mod[i][0] * 2);
					ny = c.y + (_coor_mod[i][1] * 2);
					if (nx >= 0 && nx < Map::HEIGHT && ny >= 0 && ny < Map::WIDTH &&
						_map->getType(nx, ny) == S_EMPTY)
					{
						nx = c.x + _coor_mod[!i][0];
						ny = c.y + _coor_mod[!i][1];
						if (nx >= 0 && nx < Map::HEIGHT && ny >= 0 && ny < Map::WIDTH &&
							_map->getType(nx, ny) == !stone)
						{
							_rbf_last_turn = 1;
							_last_block = side;
							_last_stone = stone;
							_last_turn = queue;
							return (false);
						}
					}
				}
			}
		}
	}
	return true;
}
