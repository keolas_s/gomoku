#pragma once

#include <cstdint>
#include <vector>
#include  "IMinMaxObject.hh"

class MinMax
{
public:
	const	static	int_fast8_t	DEPTH = 2;
	const	static	int_fast8_t	NB_OF_SIMULATION = 50;
public:
	MinMax();
	~MinMax();

	Coor	Start(IMinMaxObject *  obj);
	int	Max(IMinMaxObject  * obj, int_fast8_t depth);
	int	Min(IMinMaxObject  * obj, int_fast8_t depth);
private:
	Coor	_result;
	int_fast8_t	_max_depth;
};

#pragma region Common
MinMax::MinMax() :
_result(0,0),
_max_depth(DEPTH)
{
}

MinMax::~MinMax()
{
}


#pragma endregion


Coor	MinMax::Start(IMinMaxObject * obj)
{
	if (obj->getCount() < 4)
		_max_depth = 1;
	Max(obj, _max_depth);
	std::cout << "[" << (int)_result.x << ":" << (int)_result.y << "]" << std::endl;
	return	_result;
}

int	MinMax::Max(IMinMaxObject  * obj, int_fast8_t depth)
{
	int	max  = -10000;
	int temp_i;
	IMinMaxObject  * cur = obj->CreateCopy();
	IMinMaxObject  * temp = NULL;
	IMinMaxObject  * answer = NULL;

//	std::cout << "Max: " << (int)depth << std::endl;
	if (depth == 0 || obj->End() == true)
	{
		return (obj->Eval());
	}
	for (uint_fast8_t i = NB_OF_SIMULATION; i > 0; --i)
	{
		temp = obj->CreateTry();
		if ((temp_i = Min(temp, depth - 1)) > max)
		{
			max = temp_i;
			delete answer;
			answer = temp;
		}
		else
		{
			delete temp;
		}
	}
	if (depth == _max_depth)
	{
		_result = answer->getResult();
	}
	delete cur;
	delete answer;
	return max;
}

int	MinMax::Min(IMinMaxObject  * obj, int_fast8_t depth)
{
	int	min = 10000;
	int temp_i;
	IMinMaxObject  * cur = obj->CreateCopy();
	IMinMaxObject  * temp = NULL;
	IMinMaxObject  * answer = NULL;

//	std::cout << "Min: " << (int)depth << std::endl;
	if (depth == 0 || obj->End() == true)
	{
		return (obj->Eval());
	}
	for (uint_fast8_t i = NB_OF_SIMULATION; i > 0; --i)
	{
		temp = obj->CreateTry();
		if ((temp_i = Max(temp, depth - 1)) < min)
		{
			min = temp_i;
			delete answer;
			answer = temp;
		}
		else
		{
			delete temp;
		}
	}
	delete cur;
	delete answer;
	return min;
}
