#pragma	once

#include <cstdint>

enum e_ordinate : uint_fast8_t
{
	O_UP = 0,
	O_DOWN = 1,
	O_LEFT = 2,
	O_RIGHT = 3,
	O_UL = 4, /* ... */
	O_DL = 5, /* ... */
	O_UR = 6, /* Up Right */
	O_DR = 7, /* ... */
	O_NONE
};